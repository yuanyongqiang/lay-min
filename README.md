# layMin

#### 介绍
一个基于jquery的简单组件，layMin有简洁的提示框、弹窗、和图片预览、及加载效果等；支持作为layui的组件引入，也支持单独引入layMin.css和layMin.js


#### 安装教程

 **1.  单独使用（需要基于jquery）** 

css
```
<link rel="stylesheet" href="layMin/layMin.css" />
```
JavaScript

```
<script type="text/javascript" src="js/jquery-2.1.1.min.js" ></script>
<script type="text/javascript" src="layMin/layMin.js" ></script>
```
使用方式

```
layMin.tips({msg: "操作成功", offset: 'right', iconIndex: 0});
```


 **2.  作为layui组件使用** 

css

```
<link rel="stylesheet" href="layui/css/layui.css" />
```
JavaScript

```
<script type="text/javascript" src="layui/layui.js" ></script>
```
使用方式

```
/** 引入组件*/
layui.config({
    base: "layMin/" // 包
}).extend({
    layMin: "layMin" // 组件js
});
var $, layMin;
layui.use(['jquery', 'layMin'], function(){
    // 上面引入组件，这里引入使用
    $ = layui.jquery;
    layMin = layui.layMin;
    
    layMin.tips({msg: "操作成功", iconIndex: 0});

});
```


#### 使用说明

下载后打开 **index.html** 查看示例

#### 效果图
![输入图片说明](layMin/img/1711617370021.jpg)
![输入图片说明](layMin/img/1661140856933.jpg)
![输入图片说明](layMin/img/1688001636317.jpg)
![输入图片说明](layMin/img/1711617763332.jpg)
![输入图片说明](layMin/img/1688002743205.jpg)
![输入图片说明](layMin/img/1688002666283.jpg)
![输入图片说明](layMin/img/1688002713433.jpg)

上下左右侧弹出、弹出中间、全屏弹出、确认框弹出、弹出指定位置....

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
