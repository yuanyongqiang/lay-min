/**
 * layMin弹出框简单版 v 1.8
 *
 * 由后端开发人员编写
 * @author yyq
 * @date 2024-03-28
 */
/**
 * -------------------------------------------------------------------------------------------------------------------------------
 * layMin组件-使用方式参考index.html
 * elem: 指定标签内（默认body）
 *
 */
var layMin = {
	indexList: [],
	indexLayerList: [],
	index: 9999999999,
	animArray: ['lay-min-anim-downbit', 'lay-min-anim-upbit', 'lay-min-anim-left', 'lay-min-anim-right', 'lay-min-anim-scaleSpring', 'lay-min-anim-fadein'],
	iconArray: [
		{
			icon: '&#xe8c3;',
			color: '#53cd53',
		},{
			icon: '&#xe8c5;',
			color: '#ff5722',
		},{
			icon: '&#xe763;',
			color: '#ffb100',
		},{
			icon: '&#xe664;',
			color: '#42a5ff',
		},{
			icon: '&#xe666;',
			color: '#000000',
		},
	],
	// 图片预览
	photo: function(data){
		layMin.index = layMin.index + 1;
		var layIndex = layMin.index;
		layMin.indexList.push(layIndex);
		var s = '';
		s += '<div class="lay-min-photo-bg lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';"></div>';
		s += '<div class="lay-min-photo-box lay-min-index-'+layIndex+'" style="z-index:'+(layIndex + 1)+'">';
		s += '	<img src="'+data.src+'">';
		s += '</div>';
		$(data.elem || "body").append(s);
		var loadIndex = layMin.load({elem: ".lay-min-index-"+layIndex, type: 1});
		// 点击关闭
		$('.lay-min-index-'+layIndex).on("click", function(){
			layMin.close(layIndex);
			layMin.stopPropagation();
		})
		// 图片预览-鼠标滚动缩放
		$(document).on("mousewheel DOMMouseScroll", ".lay-min-photo-box img", function (e) {
		    var delta = (e.originalEvent.wheelDelta && (e.originalEvent.wheelDelta > 0 ? 1 : -1)) || // chrome & ie
		        (e.originalEvent.detail && (e.originalEvent.detail > 0 ? -1 : 1)); // firefox
		    var image = $(".lay-min-photo-box img");// 弹出的图片
		    var h = image.height();
		    var w = image.width();
		    if (delta > 0) {
		        if (h < (window.innerHeight)) {
		            h = h * 1.05;
		            w = w * 1.05;
		        }
		    } else if (delta < 0) {
		        if (h > 100) {
		            h = h * 0.95;
		            w = w * 0.95;
		        }
		    }
		    image.height(h);
		    image.width(w);
		});
		var img = new Image();
		img.src = data.src;
		img.onload = function(e){
			layMin.close(loadIndex, 10);
			var imgWidth = this.width;
			var imgHeight = this.height;
			var w = getLayWidth();
			var h = getLayHeight();
			var obj = layMin.setWH(w, imgWidth, imgHeight, 1);// 计算宽度（不得超出浏览器宽度）
			imgWidth = obj.imgWidth;
			imgHeight = obj.imgHeight;
			var obj = layMin.setWH(h, imgWidth, imgHeight, 2);// 计算高度（不得超出浏览器高度）
			imgWidth = obj.imgWidth;
			imgHeight = obj.imgHeight;
			$('.lay-min-index-'+layIndex).find("img").css({'width': imgWidth+'px', 'height': imgHeight+'px'});
		};
		// 成功回调
		if(data.success){
			data.success();
		}
		return layIndex;
	},
	setWH: function(wh, imgW, imgH, type){ // 图片宽高缩小计算
		if(type == 1){
			if(imgW >= wh){
				var tempW = imgW / 2;
				var tempH = imgH / 2;
				imgW = (tempW / 2) + tempW;
				imgH = (tempH / 2) + tempH;
				return layMin.setWH(wh, imgW, imgH, type);
			}else{
				return {'imgWidth': imgW, 'imgHeight': imgH};
			}
		}else{
			if(imgH >= wh){
				var tempW = imgW / 2;
				var tempH = imgH / 2;
				imgW = (tempW / 2) + tempW;
				imgH = (tempH / 2) + tempH;
				return layMin.setWH(wh, imgW, imgH, type);
			}else{
				return {'imgWidth': imgW, 'imgHeight': imgH};
			}
		}
	},
	// 提示框
	tips: function(data){
		if(!data){
			data = {};
		}
		if(data.time && data.time != 0){
			data.time = data.time > 60 ? 60 : data.time;
		}else if(data.time == 0){
			data.time = null;
		}else{
			data.time = 3;
		}
		var offset = (data.offset || 'top');
		var len = $(".lay-min-tips-"+offset).length;
		if(len < 1){
			$(data.elem || "body").append('<div class="lay-min-tips '+("lay-min-tips-"+offset)+'" style="z-index:'+(layMin.index+1000)+';'+data.style+'"></div>');
		}
		layMin.index = layMin.index + 1;
		var layIndex = layMin.index;
		layMin.indexList.push(layIndex);
		var icon = '';
		if(data.iconIndex || data.iconIndex == 0){
			var iconObj = layMin.iconArray[data.iconIndex];
			icon = '<i class="lay-min-icon lay-min-tips-icon" style="color:'+iconObj.color+';">'+iconObj.icon+'</i>';
		}
		var s = '';
		if(offset == 'top'){
			s += '<div class="lay-min-tips-box lay-min-anim '+layMin.animArray[0]+' lay-min-tips-box-'+offset+' lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';">';
		}else if(offset == 'bottom'){
			s += '<div class="lay-min-tips-box lay-min-anim '+layMin.animArray[1]+' lay-min-tips-box-'+offset+' lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';">';
		}else{
			s += '<div class="lay-min-tips-box '+(data.title?'lay-min-tips-right-bold':'')+' '+(data.anim?'lay-min-tips-box-anim':'')+' lay-min-tips-box-'+offset+' '+(data.animBg?'lay-min-tips-box-anim-bg':'')+' lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';">';
		}
		s += '	<div style="'+(data.timeDesc ? "padding: 12px 60px 10px 10px;" : "padding: 12px 18px"+(data.title?' 12px 45px':'')+";")+'">';
		s += (data.icon || icon);
		if(data.title){
			s += '		<div class="lay-min-tips-title">'+data.title+'</div>';
			s += '		<div class="lay-min-tips-text">'+(data.msg || '')+'</div>';
		}else{
			s += '		<span class="lay-min-tips-text">'+(data.msg || '')+'</span>';
		}
		s += '		<div class="lay-min-tips-line lay-min-tips-line-'+layIndex+'" style="'+(data.line ? "display:block" : "display:none")+'"></div>';
		if(data.closeIcon || (!data.closeIcon && data.closeIcon !== false)){
			s += '		<i class="lay-min-icon lay-min-tips-close lay-min-tips-close-'+layIndex+'" title="关闭">&#xeb6a;</i>';
		}
		s += '		<span class="lay-min-tips-time-desc lay-min-tips-time-desc-'+layIndex+'" style="'+(data.timeDesc ? "display:block" : "display:none")+'">'+(data.time || 3)+'</span>';
		s += '	</div>';
		s += '</div>';
		if(offset == 'left-bottom' || offset == 'bottom' || offset == 'right-bottom'){
			$(".lay-min-tips-"+offset).prepend(s);
		}else{
			$(".lay-min-tips-"+offset).append(s);
		}
		// 点击关闭
		$('.lay-min-tips-close-'+layIndex).on("click", function(){
			layMin.close(layIndex, 50);
			layMin.stopPropagation();
		})
		if(offset == 'left'){
			setTimeout(function(){
				$('.lay-min-index-'+layIndex).css("left", "0%");
			}, 50)
		}else if(offset == 'right'){
			setTimeout(function(){
				$('.lay-min-index-'+layIndex).css("right", "0%");
			}, 50)
		}else if(offset == 'left-bottom'){
			setTimeout(function(){
				$('.lay-min-index-'+layIndex).css("left", "0%");
			}, 50)
		}else if(offset == 'right-bottom'){
			setTimeout(function(){
				$('.lay-min-index-'+layIndex).css("right", "0%");
			}, 50)
		}
		// 倒计时关闭
		if(data.time){
			var tipsInterval;
			if(data.line){
				var timeNum = 0;
				var tempValue = 100;
				var tempTimeDesc = data.time;
				data.time = data.time*1000;
				tipsInterval = setInterval(function(){
					timeNum = timeNum + 10;
					var value = 10 / data.time * 100;
					tempValue = tempValue - value;
					$('.lay-min-tips-line-'+layIndex).css("width", tempValue+"%");
					if(timeNum%1000 == 0){
						tempTimeDesc --;
						$('.lay-min-tips-time-desc-'+layIndex).text(tempTimeDesc);
					}
					if(timeNum >= data.time){
						clearInterval(tipsInterval);
						layMin.close(layIndex, null);
					}
				}, 10)
				// 鼠标放上，移开事件（暂停倒计时使用）
				if(data.stype){
					$('.lay-min-index-'+layIndex).hover(function() {
						clearInterval(tipsInterval);
					}, function() {
						if(data.stype == 1){
							tipsInterval = setInterval(function(){
								timeNum = timeNum + 10;
								var value = 10 / data.time * 100;
								tempValue = tempValue - value;
								$('.lay-min-tips-line-'+layIndex).css("width", tempValue+"%");
								if(timeNum%1000 == 0){
									tempTimeDesc --;
									$('.lay-min-tips-time-desc-'+layIndex).text(tempTimeDesc);
								}
								if(timeNum >= data.time){
									clearInterval(tipsInterval);
									layMin.close(layIndex, null);
								}
							}, 10)
						}
					})
				}
			}else{
				var timeNum = 0;
				var tempTimeDesc = data.time;
				tipsInterval = setInterval(function(){
					timeNum ++;
					tempTimeDesc --;
					$('.lay-min-tips-time-desc-'+layIndex).text(tempTimeDesc);
					if(data.time == timeNum){
						clearInterval(tipsInterval);
						layMin.close(layIndex, null);
					}
				}, 1000)
				// 鼠标放上，移开事件（暂停倒计时使用）
				if(data.stype){
					$('.lay-min-index-'+layIndex).hover(function() {
						clearInterval(tipsInterval);
					}, function() {
						if(data.stype == 1){
							tipsInterval = setInterval(function(){
								timeNum ++;
								tempTimeDesc --;
								$('.lay-min-tips-time-desc-'+layIndex).text(tempTimeDesc);
								if(data.time == timeNum){
									clearInterval(tipsInterval);
									layMin.close(layIndex, null);
								}
							}, 1000)
						}
					})
				}
			}
		}
		// 点击事件
		if(data.click){
			$('.lay-min-index-'+layIndex).css('cursor', 'pointer');
			$('.lay-min-index-'+layIndex).on('click', function(){
				data.click(this);
				layMin.stopPropagation();
			})
		}
		// 成功回调
		if(data.success){
			data.success();
		}
		return layIndex;
	},
	loadSubmit: function(data){
		$(data.elem).addClass("lay-min-btn-disabled");
        $(data.elem).html('<i class="lay-min-icon lay-min-anim lay-min-anim-rotate lay-min-anim-loop">&#xe8fd;</i>'+data.msg);
	},
	closeSubmit: function(data){
		$(data.elem).html(data.msg);
        $(data.elem).removeClass("lay-min-btn-disabled");
	},
	load: function(data){
		if(!data){
			data = {};
		}
		data.bg = data.bg || "";
		data.windth = data.windth || 80;
		data.height = data.height || 35;
		data.fontSize = data.fontSize || 35;
		data.fontSizeText = data.fontSizeText || 14;
		data.color = data.color || "#76a6ff";
		data.layIcon = data.icon == 1 ? '&#xe8fc;' : '&#xe8fd;';
		layMin.index = layMin.index + 1;
		var layIndex = layMin.index;
		layMin.indexList.push(layIndex);
		var s = '';
		if(!data.type || data.type == 1){
			if(data.bg){
				s += '<div class="lay-min-shade lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';background: '+data.bg[0]+';opacity: '+(data.bg.length > 1 ? data.bg[1] : 1)+';"></div>';
			}
			s += '<div class="lay-min-load-1-1 lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';width:'+data.windth+'px;height:'+data.height+'px;text-align: center;">';
			s += '	<i class="lay-min-icon lay-min-anim lay-min-anim-rotate lay-min-anim-loop" style="color:'+data.color+';font-size: '+data.fontSize+'px;">'+data.layIcon+'</i>';
			if(data.text){
				s += '<div style="color: '+data.color+';font-size: '+data.fontSizeText+'px;margin-top: 5px;">'+data.text+'</div>';
			}
			s += '</div>';
		}else if(data.type == 2){
			if(data.bg){
				s += '<div class="lay-min-shade lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';background: '+data.bg[0]+';opacity: '+(data.bg.length > 1 ? data.bg[1] : 1)+';"></div>';
			}
			s += '<div class="lay-min-load-2-main lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';">';
			s += '	<div class="lay-min-load-2-section">';
			s += '		<div class="lay-min-load-2-child  lay-min-load-2-anim-scale">';
			s += '			<div class="lay-min-load-2-cont">';
			s += '				<i style="background-color:'+data.color+';width:'+data.fontSize+'px;height:'+data.fontSize+'px;"></i>';
			s += '				<i style="background-color:'+data.color+';width:'+data.fontSize+'px;height:'+data.fontSize+'px;" class="lay-min-load-2-layerload"></i>';
			s += '				<i style="background-color:'+data.color+';width:'+data.fontSize+'px;height:'+data.fontSize+'px;"></i>';
			s += '			</div>';
			s += '		</div>';
			s += '	</div>';
			s += '</div>';
		}else if(data.type == 3){
			if(data.bg){
				s += '<div class="lay-min-shade lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';background: '+data.bg[0]+';opacity: '+(data.bg.length > 1 ? data.bg[1] : 1)+';"></div>';
			}
			s += '<div class="lay-min-load-3 lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';">';
			s += '	<div class="lay-min-load-3-inner"></div>';
			s += '</div>';
		}else if(data.type == 4){
			if(data.bg){
				s += '<div class="lay-min-shade lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';background: '+data.bg[0]+';opacity: '+(data.bg.length > 1 ? data.bg[1] : 1)+';"></div>';
			}
			s += '<div class="lay-min-load-4 lay-min-index-'+layIndex+'" style="z-index:'+layIndex+';">';
			s += '	<span></span>';
			s += '	<span></span>';
			s += '	<span></span>';
			s += '	<span></span>';
			s += '</div>';
		}
		$(data.elem || "body").append(s);
		// 是否设置多少毫秒关闭
		if(data.time){
			setTimeout(function(){
				layMin.close(layIndex);
			}, data.time)
		}
		// 成功回调
		if(data.success){
			data.success();
		}
		return layIndex;
	},
	close: function(index, time){
		var cls = $(".lay-min-index-"+index).attr('class');
		if(cls.indexOf('lay-min-tips-box-top') != -1 || cls.indexOf('lay-min-tips-box-bottom') != -1){
			$(".lay-min-index-"+index).addClass(layMin.animArray[(cls.indexOf('lay-min-tips-box-top') != -1 ? 0 : 1)]+"-out");
		}else if(cls.indexOf('lay-min-tips-right-bold') != -1){
			$(".lay-min-index-"+index).addClass("lay-min-anim lay-min-anim-downbit-out");
		}else{
			$(".lay-min-index-"+index).fadeOut(time || 200);
		}
		setTimeout(function(){
			$(".lay-min-index-"+index).remove();
		}, 200)
		for(var i=0; i<layMin.indexList.length; i++){
			if(layMin.indexList[i] == index){
				layMin.indexList.splice(i, 1);
				break;
			}
		}
	},
	closeAll: function(time){
		if(layMin.indexList.length > 0){
			for(var i=0; i<layMin.indexList.length; i++){
				var index = layMin.indexList[i];
				$(".lay-min-index-"+index).fadeOut(time || 500);
				layMin.removeTime(index);
				layMin.indexList.splice(i, 1);
				return layMin.closeAll(time);
			}
		}
	},
	removeTime: function(index){
		setTimeout(function(){
			$(".lay-min-index-"+index).remove();
		}, 500)
	},
	stopPropagation: function(e) {// 阻止父级被子级点击触发
        e = e || window.event;
        if(e.stopPropagation) { //W3C阻止冒泡方法
            e.stopPropagation();
        } else {
            e.cancelBubble = true; //IE阻止冒泡方法
        }
    },
    // 弹窗
    openLayer: function(data, label){
		var t = 50, b = 60;
		if(data.title === false){
			t = 0;
		}
		if(data.btns === false){
			b = 0;
		}
		var tb = t + b;
		var shadeIndex = layMin.index + 1;
		layMin.index = layMin.index + 2;
		var layIndex = layMin.index;
		layMin.indexLayerList.push(layIndex);
		var eleIndex = 'lay-min-index-'+layIndex;
		// 计算宽高
		var result = layMin.layMinLayerWH(data, tb);
		var anims = "lay-min-anim-downbit";
		if(data.anim){
			anims = layMin.animArray[data.anim];
		}else if(data.anim === false){
			anims = '';
		}
		// 背景遮罩
		var shade_str = '';
		if(data.shade){
			shade_str = '<div class="lay-min-layer-shade lay-min-layer-shade-'+layIndex+'" style="background: #000000;opacity: '+data.shade+';z-index:'+shadeIndex+';"></div>';
		}
		// 主体
		var box = $('<div id="'+eleIndex+'" lay-type="'+data.type+'" lay-label="'+(data.type==2?data.content:'')+'" style="width:'+result.width+';height:'+result.height+';z-index:'+layIndex+';" lay-anim="'+anims+'" class="lay-min-layer lay-min-anim '+anims+' lay-min-layer-'+layIndex+' '+eleIndex+' '+label+'"></div>');
		// 头部标题
		var t_str = '';
		// 底部按钮
		var b_str = '';
		// 内容块
		var content_str = '';
		// 底部可拖动大小块
		var block_str = '	<div class="lay-min-layer-block lay-min-layer-block-'+layIndex+'"></div>';
		// 右下角拖动时遮罩内容
		var sc_str = '	<div id="lay-min-layer-temp-'+layIndex+'" class="lay-min-layer-temp"></div>';
		// 标题块
		if(data.title === false){
			if(data.colseIcon != false){
				t_str += '		<span class="lay-min-layer-title-close">';
				t_str += '			<i class="lay-min-icon lay-min-icon-close" title="关闭">&#xeb6a;</i>';
				t_str += '		</span>';
			}
		}else{
			data.title = data.title || '';
			var tborder = '';
			if(data.tbBorder && data.tbBorder[0] === false){
				tborder = 'border: none;'
			}
			t_str += '	<div class="lay-min-layer-title lay-min-layer-title-'+layIndex+'" style="'+tborder+'">';
			t_str += '		<span class="lay-min-layer-title-left">'+data.title+'</span>';
			t_str += '		<span class="lay-min-layer-title-right">';
			if(data.minMax){
				t_str += '			<i class="lay-min-icon lay-min-icon-screen" title="全屏">&#xe65d;</i>';
			}
			if(data.colseIcon != false){
				t_str += '			<i class="lay-min-icon lay-min-icon-close" title="关闭">&#xeb6a;</i>';
			}
			t_str += '		</span>';
			t_str += '	</div>';
		}
		// 内容块
		if(data.type == 3){
			content_str += '	<iframe class="lay-min-layer-content lay-min-layer-content-'+layIndex+'" style="border: none;height:'+result.cheight+';width: 100%;" src="'+data.content+'"></iframe>';
		}else if(data.type == 2){
			content_str += '	<div class="lay-min-layer-content lay-min-layer-content-'+layIndex+'" style="height:'+result.cheight+';"></div>';
		}else {
			content_str += '	<div class="lay-min-layer-content lay-min-layer-content-'+layIndex+'" style="height:'+result.cheight+';">'+data.content+'</div>';
		}
		// 按钮块
		if(data.btns !== false){
			var bborder = '';
			if(data.tbBorder && data.tbBorder[1] === false){
				bborder = 'border: none;'
			}
			b_str += '	<div class="lay-min-layer-bottom" style="'+bborder+'">';
			if(data.btns && data.btns.newArr && data.btns.newArr.length > 0){
				for(var i=0; i<data.btns.newArr.length; i++){
					b_str += data.btns.newArr[i];
				}
			}else{
				if(data.btns && data.btns.oldArr && data.btns.oldArr.length > 0){
					if(data.btns.oldArr.length >= 1){
						b_str += '		<a class="lay-min-layer-btn lay-min-layer-btn-yes">'+data.btns.oldArr[0]+'</a>';
					}
					if(data.btns.oldArr.length >= 2){
						b_str += '		<a class="lay-min-layer-btn lay-min-layer-btn-not">'+data.btns.oldArr[1]+'</a>';
					}
				}else{
					b_str += '		<a class="lay-min-layer-btn lay-min-layer-btn-yes">确认</a>';
					b_str += '		<a class="lay-min-layer-btn lay-min-layer-btn-not">取消</a>';
				}
			}
			b_str += '	</div>';
		}
		if(data.type == 2){
			var label = $(data.content);
			label.css('display', 'block');
			label.wrap(content_str);
			label.parent().wrap(box);
			label.parents("."+eleIndex).before(shade_str);
			label.parents("."+eleIndex).prepend(t_str);
			label.parents("."+eleIndex).append(sc_str);
			label.parents("."+eleIndex).append(b_str);
			label.parents("."+eleIndex).append(block_str);
		}else{
			// 渲染
			box.append(t_str + content_str + sc_str + b_str + block_str);
			$(data.elem || "body").append(shade_str);
			$(data.elem || "body").append(box);
		}
		// 居中/宽高设置
		layMin.centerParent(eleIndex, data.fullScreen, tb, data.offset);
		// 有标题时才可以拖动
		if(data.title){
			// 标题拖动元素
			new Drag('.lay-min-layer-'+layIndex, '.lay-min-layer-title-'+layIndex);
		}
		// 监听变化
		$(window).resize(function(){
			if(!data.fullScreen){
				var result = layMin.layMinLayerWH({'width':data.width, 'height': data.height}, tb);
				$("#"+eleIndex).css({'width': result.width, 'height': result.height});
				$("#"+eleIndex).find('.lay-min-layer-content').css('height', result.cheight);
			}
			// 居中/宽高设置
			layMin.centerParent(eleIndex, data.fullScreen, tb, data.offset);
		})
		// 右下角拖动放大
		var box = document.querySelector('.lay-min-layer-'+layIndex);
		var content = document.querySelector('.lay-min-layer-content-'+layIndex);
		var block = document.querySelector('.lay-min-layer-block-'+layIndex);
		var tempBox = document.getElementById("lay-min-layer-temp-"+layIndex);
		block.onmousedown = function(ev) {
			var myEvent = ev || event;
			var distanceX = myEvent.clientX - block.offsetLeft;
			var distanceY = myEvent.clientY - block.offsetTop;
			tempBox.style.display = 'block';
			document.onmousemove = function(ev) {
				var myEvent = ev || event;
				box.style.width = myEvent.clientX - distanceX + block.offsetWidth + 'px';
				var h = myEvent.clientY - distanceY + block.offsetHeight;
				box.style.height = h + 'px';
				content.style.height = (h - tb) + 'px';
			};
			document.onmouseup = function() {
				document.onmousemove = null;
				document.onmouseup = null;
				tempBox.style.display = 'none';
			};
		};
		// 点击全屏
		if(data.minMax){
			$("#"+eleIndex).find('.lay-min-icon-screen').click(function(){
				var value = $(this).attr("title");
				if(value == '全屏'){
					$(this).attr("title", '退出全屏');
					$(this).html("&#xe692;");
					data.fullScreen = true;
				}else{
					$(this).attr("title", '全屏');
					$(this).html("&#xe65d;");
					data.fullScreen = false;
					var result = layMin.layMinLayerWH({'width':data.width, 'height': data.height}, tb);
					$("#"+eleIndex).css({'width': result.width, 'height': result.height});
					$("#"+eleIndex).find('.lay-min-layer-content').css('height', result.cheight);
				}
				layMin.centerParent(eleIndex, data.fullScreen, tb, data.offset);
			})
		}
		// 点击遮罩关闭
		if(data.shadeClose){
			$('.lay-min-layer-shade-'+layIndex).click(function(){
				layMin.closeLayer(layIndex);
				if(data.close){
					data.close(layIndex, $("#"+eleIndex));
				}
			})
		}
		// 头部关闭回调
		$("#"+eleIndex).find('.lay-min-icon-close').click(function(){
			layMin.closeLayer(layIndex);
			if(data.close){
				data.close(layIndex, $("#"+eleIndex));
			}
		})
		// 按钮确认回调
		$("#"+eleIndex).find('.lay-min-layer-btn-yes').click(function(){
			if(data.yes){
				data.yes(layIndex, $("#"+eleIndex));
			}
		})
		// 按钮关闭回调
		$("#"+eleIndex).find('.lay-min-layer-btn-not').click(function(){
			if(data.not){
				data.not(layIndex, $("#"+eleIndex));
			}else{
				layMin.closeLayer(layIndex);
			}
		})
		// 渲染成功回调
		if(data.success){
			data.success(layIndex, $("#"+eleIndex));
		}
		// 定时关闭
		if(data.time){
			setTimeout(function(){
				layMin.closeLayer(layIndex);
			}, data.time)
		}
		return layIndex;
	},
	// 居中弹出
	/*以浏览器为基准垂直水平居中*/
	centerParent: function(eleIndex, fullScreen, tb, offset){
		if(fullScreen){
			var width = document.documentElement.clientWidth;
			var height = document.documentElement.clientHeight;
			$("#"+eleIndex).width(width+"px");
			$("#"+eleIndex).height(height+"px");
			$("#"+eleIndex).css({'left': "0px", 'top': "0px"});
			$("#"+eleIndex).find('.lay-min-layer-content').css('height', (height - tb)+'px');
		}else{
			var eleW = $("#"+eleIndex).width();
			var eleH = $("#"+eleIndex).height();
			var width = document.documentElement.clientWidth;
			var height = document.documentElement.clientHeight;
			var left = (width - eleW) / 2;
			var top = (height - eleH) / 2;
			if(offset){
				offset.left = offset.left || left;
				$("#"+eleIndex).css(offset);
			}else{
				$("#"+eleIndex).css({'left': left+"px", 'top': top+"px"});
			}
		}
	},
	/**
	 * 计算弹框的宽/高
	 * @param {Object} obj
	 */
	layMinLayerWH: function(obj, tb){
		var data = {};
		if(obj.width){
			obj.width = obj.width + "";
			if(obj.width.indexOf("%") == -1){
				var tempW = obj.width.replace('px', '');
				var w = getLayWidth();
				if(Number(tempW) > Number(w)){
					data.width = '90%';
				}else{
					data.width = obj.width+'px';
				}
			}else{
				data.width = obj.width;
			}
			if(data.width.indexOf("%") != -1){
				var tempW = data.width.replace('%', '');
				var w = getLayWidth() * Number(tempW) / 100;
				data.width = w+'px';
			}
		}
		if(obj.height){
			obj.height = obj.height + "";
			if(obj.height.indexOf("%") == -1){
				var tempH = obj.height.replace('px', '');
				var h = getLayHeight();
				if(Number(tempH) > Number(h)){
					data.height = '90%';
				}else{
					data.cheight = (Number(data.height) - tb) + 'px';
					data.height = obj.height+'px';
				}
			}else{
				data.height = obj.height;
			}
			if(data.height.indexOf("%") != -1){
				var tempH = data.height.replace('%', '');
				var h = getLayHeight() * Number(tempH) / 100;
				data.height = h+'px';
			}
			data.cheight = (Number(data.height.replace('px', '')) - tb) + 'px';
		}
		return data;
	},
	closeLayer: function(index){
		$(".lay-min-layer-shade-"+index).fadeOut(200);
		var layAnim = $(".lay-min-index-"+index).attr('lay-anim');
		$(".lay-min-index-"+index).addClass(layAnim+'-out');
		setTimeout(function(){
			var type = $(".lay-min-index-"+index).attr('lay-type');
			if(type == 2){
				var id = $(".lay-min-index-"+index).attr('lay-label');
				var label = $(id);
				label.css('display', 'none');
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-title').remove();
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-bottom').remove();
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-block').remove();
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-temp').remove();
				$(id).unwrap();
				$(id).unwrap();
			}else{
				$(".lay-min-index-"+index).remove();
			}
			$(".lay-min-layer-shade-"+index).remove();
		}, 200)
		for(var i=0; i<layMin.indexLayerList.length; i++){
			if(layMin.indexLayerList[i] == index){
				layMin.indexLayerList.splice(i, 1);
				break;
			}
		}
	},
	closeLayerAll: function(){
		if(layMin.indexLayerList.length > 0){
			for(var i=0; i<layMin.indexLayerList.length; i++){
				var index = layMin.indexLayerList[i];
				$(".lay-min-layer-shade-"+index).fadeOut(200);
				var layAnim = $(".lay-min-index-"+index).attr('lay-anim');
				$(".lay-min-index-"+index).addClass(layAnim+'-out');
				layMin.removeLayerTime(index);
				layMin.indexLayerList.splice(i, 1);
				return layMin.closeLayerAll();
			}
		}
	},
	removeLayerTime: function(index){
		setTimeout(function(){
			var type = $(".lay-min-index-"+index).attr('lay-type');
			if(type == 2){
				var id = $(".lay-min-index-"+index).attr('lay-label');
				var label = $(id);
				label.css('display', 'none');
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-title').remove();
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-bottom').remove();
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-block').remove();
				label.parents(".lay-min-index-"+index).find('.lay-min-layer-temp').remove();
				$(id).unwrap();
				$(id).unwrap();
			}else{
				$(".lay-min-index-"+index).remove();
			}
			$(".lay-min-layer-shade-"+index).remove();
		}, 200)
	},
	// 确认框
	confirm: function(data){
		var iconArr = [];
		if(!data.icon && data.icon !== false){
			data.icon = 0;
		}
		if(!data.tbBorder){
			data.tbBorder = [false, false]; // 弹框上下线条
		}
		if(!data.offset){
			data.offset = {'right': 'unset', 'bottom': 'unset','top': '20%'};
		}
		if(!data.shade && data.shade !== false){
			data.shade = 0.5;
		}
		if(data.shadeClose !== true && data.shadeClose !== false){
			data.shadeClose = true;
		}
		var s = '<div class="lay-min-layer-tips-content">';
		s += '	<i class="lay-min-icon lay-min-layer-tips-icon" style="color:'+layMin.iconArray[data.icon].color+'">'+layMin.iconArray[data.icon].icon+'</i>';
		s += '	<span class="lay-min-layer-tips-text">'+data.text+'</span>';
		s += '</div>';
		data.content = s;
		layMin.openLayer(data, 'lay-min-layer-tips');
	}
}

/**
 * -------------------------------------------------------------------------------------------------------------------------------
 * 避免$(window).width()被替换成body的内容高度，添加校验
 * @returns
 */
function getLayWidth(){
	var w1 = $(window).width();
	var w2 = window.innerWidth || document.body.clientWidth;
	if((w1 - w2) > 100){
		return w2;
	}
	return w1;
}

/**
 * -------------------------------------------------------------------------------------------------------------------------------
 * 避免$(window).height()被替换成body的内容高度，添加校验
 * @returns
 */
function getLayHeight(){
	var h1 = $(window).height();
	var h2 = window.innerHeight || document.body.clientHeight;
	if((h1 - h2) > 100){
		return h2;
	}
	return h1;
}

/**
 * -------------------------------------------------------------------------------------------------------------------------------
 * 元素拖动-标题[弹框]
 */
function Drag(selector, label){
    this.elem = typeof selector == 'object' ? selector : document.querySelector(selector);// 移动标签块
    this.elem2 = typeof selector == 'object' ? label : document.querySelector(label);// 拖动标签
    if(!this.elem2){
    	return false;
    }
    this.startX = 0;
    this.startY = 0;
    this.sourceX = 0;
    this.sourceY = 0;
    this.init();
}

Drag.prototype = {
    constructor: Drag,
    init: function(label){
        this.setDrag()
    },
    getStyle: function(property){
        return document.defaultView.getComputedStyle?document.defaultView.getComputedStyle(this.elem, null)[property]:this.elem.currentStyle[property];
    },
    getPosition: function(){
        var pos = {x:0, y:0};
        var x = parseInt(this.getStyle('left')?this.getStyle('left'):0)
        var y = parseInt(this.getStyle('top')?this.getStyle('top'):0)
        return pos = {
            x: x,
            y: y
        }
    },
    setPosition: function(pos){
    	if(pos.x < 0){
    		pos.x = 0;
    	}else{
    		var w = document.documentElement.clientWidth;
			w = w - parseInt(this.getStyle('width'));
			if(pos.x > w){
				pos.x = w;
			}
    	}
    	if(pos.y < 0){
    		pos.y = 0;
    	}else{
    		var h = document.documentElement.clientHeight;
			h = h - parseInt(this.getStyle('height'));
			if(pos.y > h){
				pos.y = h;
			}
    	}
        this.elem.style.left = pos.x + 'px';
        this.elem.style.top = pos.y + 'px';

    },
    setDrag: function(){
        var self = this;
        this.elem2.addEventListener('mousedown', start, false);
        function start(event){
            self.startX = event.pageX;
            self.startY = event.pageY;

            var pos = self.getPosition();
            self.sourceX = pos.x;
            self.sourceY = pos.y;

            document.addEventListener('mousemove', move, false);
            document.addEventListener('mouseup', end, false);
        }

        function move(event){
            var currentX = event.pageX;
            var currentY = event.pageY;
            var disX = currentX - self.startX;
            var disY = currentY - self.startY;
            self.setPosition({
                x: (self.sourceX + disX).toFixed(),
                y: (self.sourceY + disY).toFixed()
            })
        }

        function end(event){
            document.removeEventListener('mousemove', move);
            document.removeEventListener('mouseup', end);
        }
    }
}
window.Drag = Drag;

/**
 * 存在layui，就把对应的css自动填充
 */
if(typeof(layui) != "undefined" && layui != null){
	layui.define(["jquery"],function(a){
		layui.link(layui.cache.base+"/layMin/layMin.css");a("layMin", layMin);
	})
}


